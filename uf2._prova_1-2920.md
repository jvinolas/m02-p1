# UF2. Prova 1.

## Entrega

En finalitzar l'exercici s'enviarà per a la correcció:
- Els fitxers de codi (scripts) realitzats:
  - exercici1.sh
  - exercici2.sh
  - exercici3.sh -> Aquest ha de tenir la capçalera complerta!
  - exercici4.sh
  - exercici5.sh
  - exercici6.sh

## Enunciat

**Recordeu a posar el *shebang* a cada exercici. No cal que hi poseu la capçalera! Només cal indicar com serà la capçalera a l'exercici 3**

1. [1 punt] **Feu un script que retorni la data i hores actuals en format ANYMESDIA i HORAMINUT. Ha de mostrar per pantalla: *La data actual és 20200219 i l'hora 0840.***

   ```
   $./exercici1.sh
   La data actual és 20200217 i l'hora 0840
   ```

2. [2 punt] **Feu un script que comprovi si tenim feta la còpia de seguretat del directori /etc en un directori /opt/ANYMESDIA corresponent a la data d'avui.. Si el directori existeix feu que informi a l'usuari. En cas que no existeixi feu que es faci la còpia de seguretat del directori /etc en aquest directori a /opt/ANYMESDIA amb la utilitat rsync i informi.**

   ```
   $./exercici2.sh
   S'ha realitzat correctament la còpia del directori /etc al directori /opt/20200219
   ```

3. [1 punt] **Feu un script que demani un número entre 1 i 20 a l'usuari. Mentre el número que ens posi no sigui l'11 li indicarem que no ha encertat fins que introdueixi el número 11. En aquest exercici cal realitzar la capçalera complerta que descriu l'script.**

   ```
   $./exercici3.sh
   Intenta endevinar el número...
   7
   El número 7 no és correcte! Torna-ho a intentar.
   11
   Molt bé! L'has encertat!
   ```

4. [2 punts] **Feu un script que comprovi quants fitxer (no directoris) hi ha a /etc de manera recursiva i també quants fitxers (no directoris) hi ha a /opt/ANYMESDIA corresponent a la còpia de seguretat que hem realitzat al pas 2. L'script ha d'informar d'aquesta manera:**

   ​	Al directori /etc hi ha 399 fitxers i al directori /opt/ANYMESDIA hi ha 375 fitxers.

   ​	Hi ha més fitxers al directori /etc!

   **També pot informar que la comprovació ha estat correcta si hi ha el mateix nombre de fitxers.** NOTA: Jo faria servir l'ordre find...

   ```
   $./exercici4.sh
   Al directori /etc hi ha 399 fitxers i al directori /opt/ANYMESDIA hi ha 375 fitxers.
   ```

5. [2 punt] **Partint de l'script de l'exercici 4 feu que aquest script ara permeti que se li passin un nombre qualsevol de directoris i ens informi un per un de la quantitat de fitxers que hi ha dins de cadascun.**

   ```
   $./exercici5.sh /etc /var/lib /bin
   Al directori /etc hi ha 540 fitxers
   Al directori /var/lib hi ha 375 fitxers
   Al directori /bin hi ha 301 fitxers
   ```

6. [2 punt] **Partint de l'script de l'exercici 5 feu que comprovi si els directoris que li passem existeixen. En cas que no existeixin informi a l'usuari de la següent manera:**

   ```
   $./exercici4.sh /etc /var/pepito /bin
   Al directori /etc hi ha 540 fitxers
   El directori /var/pepito no existeix
   Al directori /bin hi ha 301 fitxers
   ```

